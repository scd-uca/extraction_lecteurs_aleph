﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:html="http://www.w3.org/1999/xhtml">
<xsl:output method="text" encoding="utf-8" />
  <xsl:param name="delim" select="'&#09;'" />
  <xsl:param name="internaldelim" select="'|'" />
  <xsl:param name="quote" select="'&quot;'" />
  <xsl:param name="break" select="'&#xA;'" />
  
<xsl:template match="/">
<xsl:text>"id"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"key-01"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"key-02"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"key-03"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"key-04"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"key-05"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"name"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"gender"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"birth-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"open-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"update-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"home-library"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"note-1"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"email"</xsl:text><xsl:value-of select="$delim" />
<xsl:value-of select="$break" />
<xsl:apply-templates select="printout/section-02" />
  </xsl:template>


<!-- Template pour échapper les doubles guillemets -->
<xsl:template name="doublequotes">
	<xsl:param name = "text" />
    <xsl:choose>
        <xsl:when test="contains($text, $quote)">
            <xsl:value-of select="substring-before($text, $quote)"/>
            <xsl:text>""</xsl:text>
            <xsl:call-template name="doublequotes">
                <xsl:with-param name="text" select="substring-after($text, $quote)"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$text"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>



  <xsl:template match="section-02">
  
	
	<xsl:variable name="temp01">	
    <xsl:for-each select="z302-key-01">
	<xsl:value-of select="." />
	<xsl:if test="following-sibling::z302-key-01">
      <xsl:value-of select="$internaldelim" />
    </xsl:if>
    </xsl:for-each>
	</xsl:variable>

	<xsl:variable name="temp02">	
    <xsl:for-each select="z302-key-02">
	<xsl:value-of select="." />
	<xsl:if test="following-sibling::z302-key-02">
      <xsl:value-of select="$internaldelim" />
    </xsl:if>
    </xsl:for-each>
	</xsl:variable>

	<xsl:variable name="temp03">	
    <xsl:for-each select="z302-key-03">
	<xsl:value-of select="." />
	<xsl:if test="following-sibling::z302-key-03">
      <xsl:value-of select="$internaldelim" />
    </xsl:if>
    </xsl:for-each>
	</xsl:variable>

	<xsl:variable name="temp04">	
    <xsl:for-each select="z302-key-04">
	<xsl:value-of select="." />
	<xsl:if test="following-sibling::z302-key-04">
      <xsl:value-of select="$internaldelim" />
    </xsl:if>
    </xsl:for-each>
	</xsl:variable>


	<xsl:variable name="temp05">	
    <xsl:for-each select="z302-key-05">
	<xsl:value-of select="." />
	<xsl:if test="following-sibling::z302-key-05">
      <xsl:value-of select="$internaldelim" />
    </xsl:if>
    </xsl:for-each>
	</xsl:variable>


   
    <xsl:apply-templates select="z302-id"/>    
    <xsl:value-of select="concat($quote, normalize-space($temp01), $quote)"/>    
	<xsl:value-of select="$delim" />
	<xsl:value-of select="concat($quote, normalize-space($temp02), $quote)"/>    
	<xsl:value-of select="$delim" />
	<xsl:value-of select="concat($quote, normalize-space($temp03), $quote)"/>    
	<xsl:value-of select="$delim" />
	<xsl:value-of select="concat($quote, normalize-space($temp04), $quote)"/>  
	<xsl:value-of select="$delim" />
	
	<xsl:value-of select="concat($quote, normalize-space($temp05), $quote)"/>    
	<xsl:value-of select="$delim" />
	
	

    <xsl:apply-templates select="z302-name"/>    
	<xsl:apply-templates select="z302-gender"/>    
	<xsl:apply-templates select="z302-birth-date"/>    
	<xsl:apply-templates select="z302-open-date"/>    
	<xsl:apply-templates select="z302-update-date"/>    
	<xsl:apply-templates select="z302-home-library"/>  
	
	
    <xsl:variable name="temp-note">
	  <xsl:call-template name="doublequotes">
	     <xsl:with-param name="text" select="z302-note-1"/>
	  </xsl:call-template>
	</xsl:variable>
	
	<xsl:value-of select="concat($quote, normalize-space($temp-note), $quote)"/>    
	<xsl:value-of select="$delim" />
	
	<xsl:apply-templates select="z302-email-address"/>    
 
 
    
	
	<xsl:if test="following-sibling::*">
      <xsl:value-of select="$break" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="*">
    <xsl:value-of select="concat($quote, normalize-space(), $quote)" />
    <xsl:if test="following-sibling::*">
      <xsl:value-of select="$delim" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="text()" />

</xsl:stylesheet>
