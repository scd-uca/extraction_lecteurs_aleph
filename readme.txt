Etapes

0. Extraction des données lecteurs à partir du GUI Aleph

Service circ-05 (liste de lecteurs) : exporter tous les lecteurs de toutes les bibliothèques (date de début et fin d'inscription 00/00/0000) dans un fichier "touslecteurs"
Avec WinSCP repérer les fichiers bruts créés dans u23_1/uns51/print et les déplacer sur le PC dans le dossier step1/
Ne pas récupérer le fichier global (ex: touslecteurs) mais uniquement les fichiers partiels contenant chacun 10000 entrées (Ex: touslecteurs.1 ; touslecteurs.2 ... touslecteurs.14)

1. Récupération de la date d'extraction

2. Nettoyage des fichiers extraits d'Aleph

Suppression de la ligne ##... pour créer des fichiers XML valides
Lancer le programme step2.sh

3. Extraction de CSV dans step3 à partir des XML : champs de la table 302

Lancer le programme step3.sh

4. Extraction de CSV dans step3 à partir des XML : champs de la table 305

Lancer le programme step4.sh

5. Simplification et fusion des CSV (table 302)

Lancer le programme step5.sh

6. Créer le fichier de sortie dans step6/

Lancer le programme step6.sh

7. Nettoyer les fichiers créés avant l'étape 6

Lancer le programme step7.sh




============

Structure d'un fichier XML

<form-language>FRE</form-language>
<form-format>00</form-format>
<subject>Borrower List Report</subject>
<section-01>
    <form-date>06/11/2019</form-date>
    <sub-library-address-1-occ1>UNS51</sub-library-address-1-occ1>
</section-01>
<section-02>...</section-02>
<section-02>...</section-02>

La date d'extraction est indiquée dans section-01/form-date
Le code de la bibliothèque est indiquée dans section-01/sub-library-address-1-occ1
Chaque entrée section-02 correspond à une fiche emprunteur. Si un lecteur a plusieurs fiches emprunteur on trouvera plusieurs section-02. On a donc en général 2 entrées par lecteur, pour "SCD et Bib de composantes" et pour "BU-Antennes"

Une entrée section-02 contient des informations provenant des tables z302, z303, z304, z305, z308
L'identifiant Aleph est commun entre les tables et permet de faire la jointure : z303-id ; z304-id ; z305-id ; z308-id
- z302 : champ virtuel combinant les autres champs. Infos intéressantes mais manque la plupart des infos de z305
    - z302-id                    -> Identifiant Aleph
    - z302-key-01                -> Identifiant 1 (code barre)
    - z302-key-02                -> Identifiant 2 (répétable : n° Apogée ou Harpège). Absent de z308
    - z302-key-03                -> Identifiant 3 (répétable : tag mifare). Absent de z308
    - z302-key-04                -> Identifiant 4 (répétable : login LDAP). Absent de z308
    - z302-name                  -> NOM, PRENOM
    - z302-gender                -> M ou F
    - z302-birth-date            -> JJ/MM/AAAA
    - z302-user-library          -> UNS51 (toujours)
    - z302-open-date             -> 16/07/2018
    - z302-update-date           -> 01/07/2019
    - z302-upd-time-stamp        -> 201907010726025
    - z302-home-library          -> Bib IUT Fabron
    - z302-note-1                -> Code étape / libellé. Ex : TDFT01 - DUT INFO -S1 S2-
    - z302-email-address

- z303 : fiche adhérent (globale) : https://knowledge.exlibrisgroup.com/@api/deki/files/38781/Z303.pdf?revision=1
    - z303-id                    -> identifiant Aleph
    - z303-name                  -> NOM, PRENOM
    - z303-gender                -> M ou F
    - z303-update-date           -> date de mise à jour de la fiche JJ/MM/AAAA
    - z303-upd-time-stamp        -> timestamp de mise à jour de la fiche
    - z303-home-library          -> Bib de rattachement (en clair). Ex : "BU Médecine - Pasteur"
    - z303-note-1                -> Code étape / libellé. Ex : "MU1767 - DU ANNEE 1 Soins infirmiers en reanimation"
    - z303-birth-date            -> Date de naissance JJ/MM/AAAA
- z304 : adresse : https://knowledge.exlibrisgroup.com/@api/deki/files/38783/Z304.pdf?revision=1
    - z304-id                    -> identifiant Aleph
    - z304-email-address
- z305 : fiche emprunteur (1 par bib) : https://knowledge.exlibrisgroup.com/@api/deki/files/38784/Z305.pdf?revision=1
    - z305-id                    -> identifiant Aleph
    - z305-sub-library           -> bibliothèque d'adhésion. Ex : "SCD et Bib de composantes"
    - z305-open-date             -> date de début d'inscription. Ex : 21/11/2017
    - z305-update-date           -> date de mise à jour de la fiche
    - z305-upd-time-stamp        -> timestamp de mise à jour de la fiche
    - z305-bor-type              -> catégorie statistique. Ex "Etudiant L1"
    - z305-bor-status            -> type emprunteur. Ex : "Etudiant L1-L2-L3"
    - z305-registration-date     -> 00000000
    - z305-expiry-date           -> date de fin d'inscription. Ex : 31/12/2099

- z308 : identifiants https://knowledge.exlibrisgroup.com/@api/deki/files/38786/Z308.pdf?revision=1
Ne contient que l'identifiant de type 01, mais les autres sont repris en z302
    - z308-id                    -> identifiant Aleph
    - z308-key-type              -> 01
    - z308-key-data              -> identifiant. Ex : 0021717061
    - z308-user-library          -> Ex : UNS51
    - z308-verification          -> identifiant
    - z308-verification-type     -> 00

