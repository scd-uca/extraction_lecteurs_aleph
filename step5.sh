#!/bin/bash

sourcefolder="step3"
destfolder="step5"
echo "Simplification et fusion des CSV (table 302)"


[ ! -d "$destfolder" ] && mkdir $destfolder
head -n1 -q ./$sourcefolder/*.csv | uniq > $destfolder/entete
cp $destfolder/entete $destfolder/lecteurs_302.csv
tail -n+2 -q ./$sourcefolder/*.csv >> $destfolder/lecteurs_302_temp.csv
sort $destfolder/lecteurs_302_temp.csv | uniq >> $destfolder/lecteurs_302.csv
rm $destfolder/lecteurs_302_temp.csv
rm $destfolder/entete
