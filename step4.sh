#!/bin/bash

sourcefolder="step2"
destfolder="step4"
export sourcefolder
export destfolder
echo "Extraction de CSV dans $destfolder à partir des XML : champs de la table 305"


[ ! -d "$destfolder" ] && mkdir $destfolder

echo "SCD"
find ./$sourcefolder -maxdepth 1 -name "*.xml" -type f -print0 | xargs -0 -I file sh -c 'echo "fichier $1"; xsltproc -o "${destfolder}/scd_${1##*/}.csv" mapping305_scd.xsl $1; echo "" >> "${destfolder}/scd_${1##*/}.csv"' -- file

rm $destfolder/temp.csv
head -n1 -q ./$destfolder/scd_*.csv | uniq > $destfolder/entete
cp $destfolder/entete $destfolder/allscd_lecteurs_305.csv
tail -n+2 -q ./$destfolder/scd_*.csv >> $destfolder/temp.csv
sort $destfolder/temp.csv | uniq | grep '"' >> $destfolder/allscd_lecteurs_305.csv
rm $destfolder/temp.csv
rm $destfolder/entete
rm $destfolder/scd*.csv



echo "IUT"
find ./$sourcefolder -maxdepth 1 -name "*.xml" -type f -print0 | xargs -0 -I file sh -c 'echo "fichier $1"; xsltproc -o "${destfolder}/iut_${1##*/}.csv" mapping305_iut.xsl $1; echo "" >> "${destfolder}/iut_${1##*/}.csv"' -- file

head -n1 -q ./$destfolder/iut_*.csv | uniq > $destfolder/entete
cp $destfolder/entete $destfolder/alliut_lecteurs_305.csv
tail -n+2 -q ./$destfolder/iut_*.csv >> $destfolder/temp.csv
sort $destfolder/temp.csv | uniq | grep '"' >> $destfolder/alliut_lecteurs_305.csv
rm $destfolder/temp.csv
rm $destfolder/entete
rm $destfolder/iut*.csv

echo "LJAD"
find ./$sourcefolder -maxdepth 1 -name "*.xml" -type f -print0 | xargs -0 -I file sh -c 'echo "fichier $1"; xsltproc -o "${destfolder}/ljad_${1##*/}.csv" mapping305_ljad.xsl $1; echo "" >> "${destfolder}/ljad_${1##*/}.csv"' -- file

head -n1 -q ./$destfolder/ljad_*.csv | uniq > $destfolder/entete
cp $destfolder/entete $destfolder/allljad_lecteurs_305.csv
tail -n+2 -q ./$destfolder/ljad_*.csv >> $destfolder/temp.csv
sort $destfolder/temp.csv | uniq | grep '"' >> $destfolder/allljad_lecteurs_305.csv
rm $destfolder/temp.csv
rm $destfolder/entete
rm $destfolder/ljad*.csv

echo "Villa Arson"
find ./$sourcefolder -maxdepth 1 -name "*.xml" -type f -print0 | xargs -0 -I file sh -c 'echo "fichier $1"; xsltproc -o "${destfolder}/arson_${1##*/}.csv" mapping305_arson.xsl $1; echo "" >> "${destfolder}/arson_${1##*/}.csv"' -- file

head -n1 -q ./$destfolder/arson_*.csv | uniq > $destfolder/entete
cp $destfolder/entete $destfolder/allarson_lecteurs_305.csv
tail -n+2 -q ./$destfolder/arson_*.csv >> $destfolder/temp.csv
sort $destfolder/temp.csv | uniq | grep '"' >> $destfolder/allarson_lecteurs_305.csv
rm $destfolder/temp.csv
rm $destfolder/entete
rm $destfolder/arson*.csv
