﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:html="http://www.w3.org/1999/xhtml">
<xsl:output method="text" encoding="utf-8" />
  <xsl:param name="delim" select="'&#09;'" />
  <xsl:param name="internaldelim" select="'|'" />
  <xsl:param name="quote" select="'&quot;'" />
  <xsl:param name="break" select="'&#xA;'" />
<xsl:template match="/">
<xsl:text>"id"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_sub-library"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_open-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_update-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_bor-type"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_bor-status"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_registration-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:text>"IUT_expiry-date"</xsl:text><xsl:value-of select="$delim" />
<xsl:value-of select="$break" />
<xsl:apply-templates select="printout/section-02" />
  </xsl:template>


<!-- Template pour échapper les doubles guillemets -->
<xsl:template name="doublequotes">
	<xsl:param name = "text" />
    <xsl:choose>
        <xsl:when test="contains($text, $quote)">
            <xsl:value-of select="substring-before($text, $quote)"/>
            <xsl:text>""</xsl:text>
            <xsl:call-template name="doublequotes">
                <xsl:with-param name="text" select="substring-after($text, $quote)"/>
            </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$text"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>



  <xsl:template match="section-02">
  
<xsl:if test="contains(z305-sub-library,'IUT')">


    <xsl:apply-templates select="z305-id"/>
    <xsl:apply-templates select="z305-sub-library"/>
    <xsl:apply-templates select="z305-open-date"/>
    <xsl:apply-templates select="z305-update-date"/>
    <xsl:apply-templates select="z305-bor-type"/>
    <xsl:apply-templates select="z305-bor-status"/>
	<xsl:apply-templates select="z305-registration-date"/>
	<xsl:apply-templates select="z305-expiry-date"/>

    </xsl:if>

	
	<xsl:if test="following-sibling::*">
      <xsl:value-of select="$break" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="*">
    <xsl:value-of select="concat($quote, normalize-space(), $quote)" />
    <xsl:if test="following-sibling::*">
      <xsl:value-of select="$delim" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="text()" />

</xsl:stylesheet>
