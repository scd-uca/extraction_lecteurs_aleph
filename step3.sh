#!/bin/bash

sourcefolder="step2"
destfolder="step3"
export sourcefolder
export destfolder
echo "Extraction de CSV dans $destfolder à partir des XML : champs de la table 302"


[ ! -d "$destfolder" ] && mkdir $destfolder
find ./$sourcefolder -maxdepth 1 -name "*.xml" -type f -print0 | xargs -0 -I file sh -c 'echo "fichier $1"; xsltproc -o "${destfolder}/${1##*/}.csv" mapping302.xsl $1; echo "" >> "${destfolder}/${1##*/}.csv"' -- file

