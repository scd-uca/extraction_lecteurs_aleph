#!/bin/bash

sourcefolder="source"
destfolder="step1"
echo "Récupération de la date d'export"


[ ! -d "$destfolder" ] && mkdir $destfolder

nbdates=`grep -hP "<form-date>" $sourcefolder/* | uniq | wc -l`

if [ "$nbdates" != "1" ]
then
  echo "bug"
  exit 1
fi

grep -hP "<form-date>" $sourcefolder/* | uniq | grep -Po "[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]" | sed 's/\([0-9][0-9]\)\/\([0-9][0-9]\)\/\([0-9][0-9][0-9][0-9]\)/\3-\2-\1/g' > $destfolder/dateexport

echo "Date d'export : "
cat $destfolder/dateexport


