#!/bin/bash

sourcefolder="source"
destfolder="step2"
export sourcefolder
export destfolder
echo "Suppression de la ligne ## initiale des fichiers bruts dans $sourcefolder et création de fichiers XML valides dans $destfolder"


[ ! -d "$destfolder" ] && mkdir $destfolder
find ./$sourcefolder -maxdepth 1 ! -name ".*" -type f -print0 | xargs -0 -I file sh -c 'echo "fichier $1"; tail -n+2 $1 > "${destfolder}/${1##*/}.xml"' -- file
