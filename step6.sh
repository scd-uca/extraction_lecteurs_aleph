#!/bin/bash

sourcedateexport="step1"
sourcefolder302="step5"
sourcefolder305="step4"
destfolder="resultat"
echo "Jointure des CSV correspondant aux tables 302 et 305"

datescript=`date -I`
dateeexport=`cat $sourcedateexport/dateexport`


[ ! -d "$destfolder" ] && mkdir $destfolder


head -n1 -q ./$sourcefolder302/*.csv | uniq > $destfolder/entete302
tail -n+2 -q ./$sourcefolder302/*.csv > $destfolder/lecteurs_302.csv

head -n1 -q ./$sourcefolder305/allarson_lecteurs_305.csv | uniq > $destfolder/arson_entete305
head -n1 -q ./$sourcefolder305/allscd_lecteurs_305.csv | uniq > $destfolder/scd_entete305
head -n1 -q ./$sourcefolder305/alliut_lecteurs_305.csv | uniq > $destfolder/iut_entete305
head -n1 -q ./$sourcefolder305/allljad_lecteurs_305.csv | uniq > $destfolder/ljad_entete305

tail -n+2 -q ./$sourcefolder305/allarson_lecteurs_305.csv > $destfolder/allarson_lecteurs_305.csv
tail -n+2 -q ./$sourcefolder305/allscd_lecteurs_305.csv > $destfolder/allscd_lecteurs_305.csv
tail -n+2 -q ./$sourcefolder305/alliut_lecteurs_305.csv > $destfolder/alliut_lecteurs_305.csv
tail -n+2 -q ./$sourcefolder305/allljad_lecteurs_305.csv > $destfolder/allljad_lecteurs_305.csv


join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/entete302 $destfolder/scd_entete305 | sort > $destfolder/entetetemp1
join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/entetetemp1 $destfolder/iut_entete305 | sort > $destfolder/entetetemp2
join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27,1.28,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/entetetemp2 $destfolder/ljad_entete305 | sort > $destfolder/entetetemp3
join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27,1.28,1.29,1.30,1.31,1.32,1.33,1.34,1.35,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/entetetemp3 $destfolder/arson_entete305 | sort > $destfolder/entetetemp4



join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/lecteurs_302.csv $destfolder/allscd_lecteurs_305.csv | sort > $destfolder/temp1.csv
join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/temp1.csv $destfolder/alliut_lecteurs_305.csv | sort > $destfolder/temp2.csv
join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27,1.28,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/temp2.csv $destfolder/allljad_lecteurs_305.csv | sort > $destfolder/temp3.csv
join -t $'\t' -a 1 -a 2 -e VIDE -o 1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27,1.28,1.29,1.30,1.31,1.32,1.33,1.34,1.35,2.2,2.3,2.4,2.5,2.6,2.7,2.8 $destfolder/temp3.csv $destfolder/allarson_lecteurs_305.csv | sort > $destfolder/temp4.csv

cat $destfolder/entetetemp4 $destfolder/temp4.csv > $destfolder/exportlecteurs_${dateeexport}_${datescript}.csv

rm $destfolder/temp*.csv
rm $destfolder/entete*
rm $destfolder/all*
rm $destfolder/lecteur*
rm $destfolder/arson*
rm $destfolder/iut*
rm $destfolder/scd*
rm $destfolder/ljad*


