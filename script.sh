#!/bin/bash

echo "Les listes de fichiers lecteur bruts produits par le service circ-05 (récupérés avec WinSCP) doivent être déposées dans le dossier source/. Le fichier résultat au format TSV sera créé dans un dossier resultat/"

while true; do
    read -p "Voulez-vous lancer le programme (O/N) ? " yn
    case $yn in
        [oO]* ) bash ./step1.sh
                if [ $? -ne 0 ]
                then
                  echo "erreur à l'étape 1, sortie du script !"
                  exit 1
                fi
                bash ./step2.sh
                if [ $? -ne 0 ]
                then
                  echo "erreur à l'étape 2, sortie du script !"
                  exit 1
                fi
                bash ./step3.sh
                if [ $? -ne 0 ]
                then
                  echo "erreur à l'étape 3, sortie du script !"
                  exit 1
                fi
                bash ./step4.sh
                if [ $? -ne 0 ]
                then
                  echo "erreur à l'étape 4, sortie du script !"
                  exit 1
                fi
                bash ./step5.sh
                if [ $? -ne 0 ]
                then
                  echo "erreur à l'étape 5, sortie du script !"
                  exit 1
                fi
                bash ./step6.sh
                if [ $? -ne 0 ]
                then
                  echo "erreur à l'étape 6, sortie du script !"
                  exit 1
                fi
                bash ./step7.sh
                break;;
        [Nn]* ) exit;;
        * ) echo "Répondez O ou N";;
    esac
done



